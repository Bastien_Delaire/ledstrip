//fichier ledStrop exercice 1.cpp
//fichier controle ruban
//bastien delaire /maxime Quennelle le 17/03/2021
//------------------------------------------------------------------------------------
#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>  // on inclue la blibliothèque FastLed

#define NUM_LEDS 10 //nombre de LED du ruban    //constante pour les 10 led 
#define DATA_PIN 2  //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];  // controle des leds avec la constante
int i;  // compteur 

void setup()
{
  //configuration du ruban de LEDs
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void loop()
{
  FastLED.clear();  //on eteint les led
  leds[i] = CRGB::Red;  // on rajoute sur tout les led la couleur rouge 
  FastLED.show();   // on allume les leds
  delay(500);   
  FastLED.show();   // on ralume les leds 
  i++;
  if (i == NUM_LEDS) // si i = la constante 
  {
    i = 0;  // resultat 
  }
}
