//fichier ledStrop exercice 4 partie 2.cpp
//fichier controle ruban
//bastien delaire /maxime Quennelle le 17/03/2021
//------------------------------------------------------------------------------------
#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>

#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN 2  //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];

int pos = 0;
int iLedP = 0; // leds principal initialisé a 0
int ledDroite = 0,  // led droite initialisé a 0
    ledGauche = 0; //led gauche initialisé a 0
    int lum;  //pour ajouter la couleur avec CRGB

void allumerLeds (int prmNumPrincipal){     //fonction allumer la led avec un parametre 
    FastLED.clear();    // ont eteints les leds
    ledGauche = prmNumPrincipal+1;  // led gauche = au parametre +1 pour s'allumer
    ledGauche = ledGauche % 10;     
    ledDroite = prmNumPrincipal-1; // led gauche = au parametre -1 pour eteindre 
    ledDroite = ledDroite % 10;
    iLedP =prmNumPrincipal;
    iLedP = iLedP % 10;
    leds[ledGauche] = CRGB(0, 0, 50);
    leds[iLedP] = CRGB(0, 0, 255);
    leds[ ledDroite] = CRGB(0, 0, 50);
    FastLED.show();
    lum = prmNumPrincipal*2.55; 
    lum = lum % 255;    

}

void setup()
{
    //configuration du ruban de LEDs
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void loop()
{

    allumerLeds(pos);       // on appelle la fonction avec la variable pos
    delay(150);
    pos++;
    if (pos <= 100)     // on demande a ce que si pos superieur ou egal a 100 
    {
        pos = 1;        // on remet a la position 1
    }
}