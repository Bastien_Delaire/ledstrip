//fichier ledStrop exercice 4.cpp
//fichier controle ruban
//bastien delaire /maxime Quennelle le 17/03/2021
//------------------------------------------------------------------------------------
#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>

#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN 2  //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];

int pos = 0; // variable pour la position de la led 
int iLedP = 0;  //led principal
int ledDroite = 0, // led droite    
    ledGauche = 0;  // led gauche

void setup()
{
    //configuration du ruban de LEDs
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void loop()
{
    FastLED.clear();
    ledGauche = pos+1;  // premiere led = à la premiere position
    ledGauche = ledGauche % 10;  // modulo pour placer la lumiere
    ledDroite = pos-1;
    ledDroite = ledDroite % 10;  
    iLedP = pos;
    iLedP = iLedP % 10;
    leds[ledGauche] = CRGB(0, 0, 50);  // la led gauche s'allumeras en bleu 
    leds[iLedP] = CRGB(0, 0, 255);      //  la led principal s'allumeras en bleu un peux plus clair
    leds[ ledDroite] = CRGB(0, 0, 50); // la led droite s'allumeras en bleu 
    FastLED.show();     // on l'allume les leds
    delay(150);     //on ajoute un delay
    pos++; // ils passes aux leds suivant
    if (pos <= 100) //si pos superieur ou eqal a 100
    {
        pos = 1;    // donc la led s'allumes
    }
}