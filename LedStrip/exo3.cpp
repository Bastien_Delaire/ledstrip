
//fichier ledStrop exercice 3.cpp
//fichier controle ruban
//bastien delaire /maxime Quennelle le 17/03/2021
//------------------------------------------------------------------------------------#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>

#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN 2  //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];

int iLed = 0;   // variable initialisé 
int test;   //variable de teste 
int lum; //variable pour changer la couleur de la lumiere 

void setup()
{
    //configuration du ruban de LEDs
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void loop()
{

    if (iLed == 0)
    {
        test = 1;
    }

    if (iLed == 9)
    {
        test = 2;
    }
    if (test == 1)
    {
        leds[iLed] = CHSV(lum, 150, 255);
        FastLED.show(); //allume la led 1
        delay(500);
        leds[iLed] = CRGB::Black; //attend 5s pour allume la prochaine
        FastLED.show();
        iLed++; //alume la led suivante
        lum = lum - 10;  //
    }

    if (test == 2)
    {
        leds[iLed] = CHSV(lum, 150, 255);
        FastLED.show(); //allume la led 1
        delay(500);
        leds[iLed] = CRGB::Black; //attend 5s pour allume la prochaine
        FastLED.show();
        iLed--; //alume la led suivante
        lum = lum + 10;
    }
}
